import Counter from './Counter.vue'
import GridSection from './GridSection.vue'
import Pipeline from './Pipeline.vue'
import Section from './Section.vue'
import Siderail from './Siderail.vue'
import Step from './Step.vue'
import Steps from './Steps.vue'
import SpinnerVivid from './SpinnerVivid.vue'
import Icon from './Icon.vue'

const components = [
  Counter,
  Pipeline,
  GridSection,
  Section,
  Siderail,
  Step,
  Steps,
  SpinnerVivid,
  Icon,
]

/**
 * Install method is exposed to Vue
 * @param Vue
 * @param options
 */
const install = (Vue, options = {}) => {
  components.forEach(component => {
    Vue.component(component.name, component)
  })
}

export default {
  install
}
