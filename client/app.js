import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import App from './App.vue'
import router from './router'
import store from './store'
import Element from 'element-ui'
import Components from './components'
import { ApolloClient, createNetworkInterface } from 'apollo-client'
import VueApollo from 'vue-apollo'

// Create JointJS
window.joint = require('jointjs')

sync(store, router)

/* ELEMENT-UI STARTS */
import './styles/global.scss'
Vue.use(Element)
/* ELEMENT-UI ENDS */

/* Custom Components STARTS */
Vue.use(Components)
/* Custom Components ENDS */

console.log('connect to ', process.env.GRAPHQL_PORT)
// Create the apollo client

const apolloClient = new ApolloClient({
  networkInterface: createNetworkInterface({
    uri: `http://localhost:${process.env.GRAPHQL_PORT}/graphql/`,
    opts: {
      credentials: 'same-origin'
    }
  }),
  connectToDevTools: true
})

/* APOLLO STARTS */
Vue.use(VueApollo)
const apolloProvider = new VueApollo({
  clients: {
    a: apolloClient
  },
  defaultClient: apolloClient,
  defaultOptions: {},
  watchLoading (state, mod) {
    console.log('Global loading', mod)
  },
  errorHandler (error) {
    console.log('Global error handler')
    console.error(error)
  }
})
/* APOLLO ENDS */

const app = new Vue({
  router,
  store,
  apolloProvider,
  ...App
})

export { app, router, store }
