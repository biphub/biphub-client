import Vue from 'vue'
import Router from 'vue-router'
import HomePage from '../views/HomePage.vue'
import PipelinePage from '../views/PipelinePage.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: HomePage
    },
    {
      name: 'pipeline',
      path: '/pipeline/:id',
      component: PipelinePage
    }
  ]
})
